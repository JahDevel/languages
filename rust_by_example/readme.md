# Rust By Example


The regular rust handbook is boring...


## Hello World


Print Hello World!


### Input


1. Code
```rust
// This is a comment

// This is the main function
fn main() {

    // Print to console
    println!( "Hello World!" ) ;

}
```


2. Compile
> $ rustc { filename }.rs


3. Execute
> $ ./{ filename }


### Output


```
Hello World!
```
