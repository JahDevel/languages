# Rust


Blazingly fast...
And some other stuff


## Foreword


Rust is about empowerment.
Allowing you to write quality code for a wider range of applications.


Having abstracted typically troublesome code, 'low-level' code
can be written reliably and still be efficient in speed and memory usage.


Not to mention the compiler which will help pick up on errors!


This book not only aims to empower the user with Rust,
but also knowledge that is applicable in the greater
realm of programming.


## Introduction


High-level abstraction and low-level control are naturally hard to
combine. Rust defies this in a balancing act of technical detail and
user experience.


### Who is rust for?


Rust is for everyone!


+ Teams of developers
> The compiler is a blessing for teams, where the compiler catches
>   subtle bugs. Also boasting, the rust package manager cargo for dependencies,
>   building and compiling, and lsp for ide's!

+ Students
> System concepts are easy to pick up, and many other areas of knowledge are
>   also touched. However the biggest selling point is the great community!

+ Companies
> Rust is used in production for tasks from web services to embedded devices.

+ OSS devs
> Rust loves contributors!
